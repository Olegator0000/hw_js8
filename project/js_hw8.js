// Знайти всі параграфи на сторінці та встановити колір фону #ff0000
//
//
// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
//
//
//     Встановіть в якості контента елемента з класом testParagraph наступний параграф -
// This is a paragraph
//
//
//
// Отримати елементи , вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
//
//
//     Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.


const parag = document.querySelectorAll("p")
console.log(parag)
for (const paragElement of parag) {
    console.log(paragElement)
    paragElement.style.backgroundColor = '#ff0000'
} (parag)

const optionsList = document.getElementById('optionsList')
console.dir(optionsList)
const parentElem = optionsList.parentElement
console.dir(parentElem)
for (const parentElemElement of optionsList.childNodes) {
    console.log(parentElemElement.nodeName , parentElemElement.nodeType)

}
const testParagraph = document.getElementById('testParagraph')
testParagraph.textContent = 'This is a paragraph'
const childMainHeader = document.querySelector('.main-header')
const elements = childMainHeader.children
console.log(elements)

const sectionTitles = document.querySelectorAll(".section-title")
for (const sectionTitle of sectionTitles) {
    sectionTitle.classList.remove("section-title")
}
